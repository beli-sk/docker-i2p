FROM debian:stable
MAINTAINER Michal Belica <devel@beli.sk>

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends gnupg2 dirmngr lsb-release \
 && echo "deb http://deb.i2p2.de `lsb_release -sc` main" > /etc/apt/sources.list.d/i2p.list \
 && echo "deb-src http://deb.i2p2.de `lsb_release -sc` main" >> /etc/apt/sources.list.d/i2p.list \
 && ( for ii in 1 2 3 4 5 ; do echo "Try $ii / 5:" ; apt-key adv --keyserver hkp://pool.sks-keyservers.net --recv 7840E7610F28B904753549D767ECE5605BCF1346 && break || true ; done ) \
 && apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends i2p \
 && DEBIAN_FRONTEND=noninteractive apt-get purge --auto-remove -y gnupg2 dirmngr lsb-release \
 && apt-get clean && rm -rf /var/lib/apt/lists/*

ENV I2P_ARGS /etc/i2p/wrapper.config \
	wrapper.java.additional.1=-DloggerFilenameOverride=/var/log/i2p/log-router-@.txt \
	wrapper.java.additional.10=-Dwrapper.logfile=/var/log/i2p/wrapper.log \
	wrapper.java.additional.11=-Di2p.dir.pid=/run/i2p \
	wrapper.java.additional.12=-Di2p.dir.temp=/tmp/i2p-daemon \
	wrapper.logfile=/var/log/i2p/wrapper.log \
	wrapper.pidfile=/run/i2p/i2p.pid \
	wrapper.java.pidfile=/run/i2p/routerjvm.pid \
	wrapper.daemonize=FALSE
ENV TZ UTC

RUN /bin/mkdir -p /tmp/i2p-daemon ; /bin/mkdir -p /run/i2p ; \
	/bin/chown -R i2psvc: /var/log/i2p /run/i2p /tmp/i2p-daemon ; \
	/bin/chmod 750 /var/log/i2p ; \
	echo -e 'i2np.udp.port=13768\ni2np.udp.internalPort=13768' >> /usr/share/i2p/router.config ; \
	/bin/sed -ire 's#^tunnel\.0\.interface=.*#tunnel.0.interface=0.0.0.0#' /usr/share/i2p/i2ptunnel.config ; \
	/bin/sed -ire 's#^clientApp\.0\.args=.*#clientApp.0.args=7657 0.0.0.0 ./webapps/#' /usr/share/i2p/clients.config

ADD entrypoint /entrypoint

VOLUME /var/lib/i2p

EXPOSE 7657
EXPOSE 4444
EXPOSE 13768

CMD ["/entrypoint"]
